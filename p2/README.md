# LIS 4368 Advanced Web Applications Development 

## Jackson Pence

### Deliverables


1. Updated CustomerServlet.java, customers.jsp, customerform.jsp, customerDB.java with data validation included


#### README.md file should include the following items:

* Screenshots of p2 localhost page with examples of failed/accepted data parameters 
* Update, add, and delete customers within database
* Bitbucket repo links

# Assignment Screenshots: 

## *Screenshots of P2 Page*: 

## *Add Customer*: 
![Project 2](img/p2pt1.png)

## *thanks.jsp page*:
![Project 2](img/p2pt2.png)

## *Update page*:
![Project 2](img/p2pt3.png)