# LIS 4368 Advanced Web Applications Development 

## Jackson Pence

### Deliverables


1. Updated index.jsp with data validation included
2. Skillsets 7-9


#### README.md file should include the following items:

* Screenshots of p1 localhost page with examples of failed/accepted data parameters 
* Screenshots of skillsets 7-9
* Bitbucket repo links

# Assignment Screenshots: 

## *Screenshots of P1 Page*: 

## *Invalid Data Input*: 
![Project 1](img/p1pt1.png)

## *Valid Data Input*: 
![Project 1](img/p1p2.png)


# Skillset Screenshots:

## *Skillset 7*: 

![Skillset 7](img/ss7.png)

## *Skillset 8* 

![Skillset 8](img/ss8p1.png)

![Skillset 8](img/ss8p2.png)

## *Skillset 9*

![Skillset 9](img/ss9.png)
