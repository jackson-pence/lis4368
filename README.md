# LIS4368 - Advanced Web Applications Development (Java/Apache)

## Jackson Pence

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README](a1/README.md)
    - Install JDK/Tomcat
    - Hello.java
    - Git commands
    - Tomcat install and A1 page

2. [A2 README](a2/README.md)
    - SQL and Servlet configuration
    - Skillset 1: System properties
    - Skillset 2: Looping structures
    - Skillset 3: Number swap

3. [A3 README](a3/README.md)
    - ERD and SQL script creation
    - Skillset 4: Directory info
    - Skillset 5: Character info
    - Skillset 6: Determine character

3. [P1 README](p1/README.md)
    - Client side validation of user entered data.
    - Skillset 7: Count & categorize characters in a string
    - Skillset 8: Show ASCII values
    - Skillset 9: Determine letter grade

4. [P2 README](p2/README.md)
    - Client and server side data validation
    - Add, delete, and update customer table
    