# LIS 4368 Advanced Web Applications Development 

## Jackson Pence

### Assignment 2 Requirements:

*Three Parts:*

1. Setup SQL 
2. Finish Servlet tutorial
3. Skillsets 1-3


#### README.md file should include the following items:

* Screenshot of index.html, sayhi, querybook, query results
* Screenshots of skillsets 1-3
* Bitbucket repo links



# Assignment Screenshots: 

## *Using Servlets*

![Java servlet](img/using_servlets.png)

## *SQL Database Query Servlet*

![SQL database](img/database_connectivity1.png)

## *SQL Query Servlet Results*

![SQL database](img/database_connectivity2.png)



# Skillset Screenshots:

## *Skillset1*: 

![Skillset 1](img/ss1.png)

## *Skillset 2* 

![Skillset 2](img/ss2.png)

## *Skillset 3*

![Skillset 3](img/ss3.png)