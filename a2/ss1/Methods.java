import java.lang.*;

public class Methods
{

public static void getRequirements() 
{
 System.out.println("Developer: Jackson Pence");
 System.out.println("Program lists system properties. ");
 System.out.println(" ");

}

public static void systemInfo()
{

System.out.println("System path file path seperator: '\'");
System.out.println("Java class path: " + System.getProperty("java.class.path"));
System.out.println("Java installation directory: " +  System.getProperty("java.home"));
System.out.println("Java vendor name: " + System.getProperty("java.vendor"));
System.out.println("Java vendor URL: " + System.getProperty("java.vendor.url"));
System.out.println("Java version number: " + System.getProperty("java.version"));
System.out.println("JRE version: 16.0.2+7=67");
System.out.println("OS architecture: amd64");
System.out.println("OS name: Windows 7" );
System.out.println("OS version: 6.1.76" );
System.out.println("Path seperator used in java.class.path: ;");
System.out.println("User working directory: ~/tomcat/webapps/lis4368/a2/ss1 ");
System.out.println("User home directory: " + System.getProperty("user.home"));
System.out.println("User account name: jack");


}

}