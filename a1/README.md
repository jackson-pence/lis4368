# LIS 4368 Advanced Web Applications Development 

## Jackson Pence

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installations
3. Chapter Questions (1-4)


#### README.md file should include the following items:

* Screenshot of running Java Hello 
* Screenshot of running http://localhost:9999
* Git commands w/ short descriptions
* Bitbucket repo links


> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one
2. git status: Show the working tree status
3. git add: Add file contents to the index
4. git commit: Record changes to the repository
5. git push: Update remote refs along with associated objects
6. git pull: Fetch from and integrate with another repository or a local branch
7. git merge: Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of Java Hello*: 

![Java Hello](img/jdk_install.png)

*Screenshot of Tomcat Default Page* 

![Tomcat Default Page](img/tomcat.png)

*A1 Index Page*
![A1 Site Page](img/A1.png)