# LIS 4368 Advanced Web Applications Development 

## Jackson Pence

### Deliverables


1. Entity relationship diagram (ERD)
2. 10 indexes of data in each table
3. Skillsets 4-6


#### README.md file should include the following items:

* Screenshot of ERD, indexes, .mwb and .sql files
* Screenshots of skillsets 4-6
* Bitbucket repo links



# Assignment Screenshots: 

*Screenshots A3 ERD*: 
 
![A3 ERD](img/a3.png "ERD based upon A3 Requirements") 
 

![A3 ERD](img/petstore.png "Petstore table indexes") 

![A3 ERD](img/pet.png "Pet table indexes") 

![A3 ERD](img/customer.png "Customer table indexes") 

![A3 ERD](img/engineer.png "Proof of forward engineering success on local server") 
*A3 docs: a3.mwb and a3.sql*: 
 
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format") 
 
[A3 SQL File](docs/a3.sql "A3 SQL Script") 




# Skillset Screenshots:

## *Skillset 4*: 

![Skillset 1](img/ss4.png)

## *Skillset 5* 

![Skillset 2](img/ss5.png)

## *Skillset 6*

![Skillset 3](img/ss6.png)