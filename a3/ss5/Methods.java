import java.util.Scanner;
import java.util.*;


public class Methods
{

public static void getRequirements() 
{
 System.out.println("Developer: Jackson Pence");
 System.out.println("Program determines total number of characters in a line of text,");
 System.out.println("as well as number of times a specific character is used.");
 System.out.println("Program displays character's ASCII value.");
 
}


public static void characterInfo()
{

String str = "";
char ch = ' ';

int len = 0;
int num = 0;
Scanner sc = new Scanner(System.in);

System.out.print("Please enter a line of text: ");

str = sc.nextLine();

len = str.length();

System.out.print("Please enter character to check: ");

ch = sc.next().charAt(0);

for (int i=0; i < len; i++)
{


    if(ch == str.charAt(i)){

        ++num;

    }
}

System.out.println("\nNumber of characters in line of text: "+ len);
System.out.println("The character" + ch + "appears " + num + " time(s)");
System.out.println("ASCII value: " + (int)ch);
}




}